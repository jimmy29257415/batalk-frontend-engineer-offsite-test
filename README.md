# frontend-engineer-offsite-test
[test deployment](https://batalk-frontend-test.herokuapp.com/)

## Getting Started
```bash
git clone https://gitlab.com/jimmy29257415/batalk-frontend-engineer-offsite-test.git

cd batalk-frontend-engineer-offsite-test

yarn
```
After cloning the repository, run `yarn serve` and navigate to  http://localhost:8080.

## Structure
```
src
├── App.vue
├── components
│   ├── quesItem.vue
│   ├── questions.vue
│   ├── quesUser.vue
│   └── tagBar.vue
├── imgs
│   └── meloopcard.png
└── main.js
```

## Work time
* Learning Vue, building basic function: 14hr
* Learning Vuex nad adjust structure with Vuex: 2.5hr
