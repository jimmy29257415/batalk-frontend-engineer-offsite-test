import Vue from 'vue'
import App from './App.vue'
import { Input, RadioGroup, RadioButton, Radio } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Vuex from 'vuex';

Vue.config.productionTip = false
Vue.use(Input);
Vue.use(RadioGroup);
Vue.use(RadioButton);
Vue.use(Radio);

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    tags: [],
    questions: [],
    page: 1,
    target: ''
  },
  mutations: {
    setTags(state, tags) {
      state.tags = tags
    },
    setQuestions(state, questions) {
      state.questions = questions
    },
    restartPage(state) {
      state.page = 1;
    },
    nextPage(state) {
      state.page += 1;
    },
    setTarget(state, target) {
      state.target = target
    }
  }
})

new Vue({
  render: h => h(App),
  store: store
}).$mount('#app')
